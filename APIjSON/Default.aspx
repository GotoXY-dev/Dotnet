﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="APIjSON.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h2>Exemplo Json .NET Newtonsoft</h2>
        <hr />
        <div>
        <asp:TextBox ID="TxtUrl" runat="server" Height="20px" Width="444px" Font-Size="Medium"> </asp:TextBox>
        <asp:Button ID="btnCarregar" runat="server" Text="Carregar Dados" OnClick="Button_Click" />    
        </div>
        <hr />
        <div>
            <asp:GridView ID="gdvDados" runat="server" Height="192px" Width="688px"></asp:GridView>
        </div>
    </form>
</body>
</html>
